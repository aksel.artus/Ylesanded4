package ee.bsc.valiit;

import java.util.SortedSet;
import java.util.TreeSet;

public class Ex21 {

    public static void main(String[] args) {
        TreeSet<String> newSet = new TreeSet<>();

        newSet.add("Banana");
        newSet.add("Apple");

        for (String section : newSet) {
            System.out.println(section);
        }


    }
}
