package ee.bsc.valiit;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Ex22 {

//Lahendus 1
    /*

    public static void getCountryCities() {
        HashMap<String, String[]> countries = new HashMap<>();
        String[] eestiLinnad = {"Tallinn", "Tartu", "Valga", "Võru"};
        String[] rootsiLinnad = {"Stockholm", "Uppsala", "Lund", "Köping"};
        String[] soomeLinnad = {"Helsinki", "Espoo", "Hanko", "Jämsä"};
        countries.put("Estonia", eestiLinnad);
        countries.put("Sweden", rootsiLinnad);
        countries.put("Finland", soomeLinnad);
        for (int j = 0; j < countries.size(); j++) {
            String country = countries.keySet().toArray()[j].toString();
            System.out.println("Country: " + country);
            System.out.println("Cities: ");
            for (int i = 0; i < countries.get(country).length; i++) {
                System.out.println("\t" + countries.get(country)[i]);
            }
        }
        for (String name : countries.keySet()) {
            System.out.println("Country: " + name);
            System.out.println("Cities: ");
            String[] cities = countries.get(name);
            for (String city : cities) {
                System.out.println("\t" + city);
            }
        }
    }

}
    */

    //Lahendus 2


    public static void main(String[] args) {
        Map<String, String[]> map = new HashMap<>();
        String[] estCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        map.put("Estonia", estCities);                                   // lisame hashmappi
        String[] sweCities = {"Stockholm", "Uppsala", "Lund", "Köping"}; //teeme rootsi linnade massiivi
        map.put("Sweden", sweCities);                                    //lisame hashmappi
        String[] finCities = {"Helsinki", "Espoo", "Hanko", "Jämsä"};
        map.put("Finland", finCities);

        for (Map.Entry<String, String[]> country : map.entrySet()) { //küsime mapist ühe liikme. Ehk meil on vaja saada riik sealt kätte. Country loome selle lause sees. Et ta annaks tagasi ühe riigi nime
            System.out.println("Country:" + country.getKey());       // key on riigi nimi. Hashmappis on key ja siis see object
            System.out.println("Cities");
            for (String city : country.getValue()) {                 // võtame sealt riigist tema linnad.
                System.out.println("\t" + city);

            }
        }
        map.forEach((k, v) -> {  // lamda. Et sellest mapist key value saadetakse funktsiooni
            System.out.println("Country: " + k);
            System.out.println("Cities");
            for (String city : v) {
                System.out.println("\t" + city);
            }

        });
        //Set<String> keys = map.keySet();

        //enesepiinamine. Ei kasutata.
        Object[] keys = map.keySet().toArray(); //võtame key-d ja teeme arrayks
        for (int i = 0; i < map.size(); i++) {
            if (keys[i] instanceof String) {
                System.out.println("Ongi teksti tüüp");
            }
            String key = (String) keys[i];              // castimine. Saad ühest objektist castida kui annad talle ette emaobjekti.
            System.out.println("Country: " + key);
            System.out.println("Cities: ");
            String[] values = map.get(key);
            for (int j = 0; j < map.get(key).length; j++) {
                System.out.println("\t" + values[j]);
            }
        }
    }
}
