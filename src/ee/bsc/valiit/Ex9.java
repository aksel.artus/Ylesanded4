package ee.bsc.valiit;

public class Ex9 {

    static int[][] array = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9, 0}};

    public static void main(String[] args) {


        System.out.println("length: " + array.length);


        for (int[] i : array) {
            for (int j : i)

                System.out.print(j + ", ");


        }
        System.out.println();


    }
}

