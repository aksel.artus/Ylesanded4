package ee.bsc.valiit;

import java.util.stream.IntStream;

public class Ex14 {
    public static void main(String[] args) {

        IntStream ranges = IntStream.range(1, 100);
        int[] intArray = ranges.toArray();
        for (int i = 3; i < intArray.length; i = i + 3) {
            System.out.println(i);
        }
    }
}
