package ee.bsc.valiit;

import java.lang.reflect.Array;

public class Ex10 {

    public static void main(String[] args) {


        String[][] array = {{"Estonia: "}, {"Tallinn", "Tartu", "Valga", "Võru"},{"Sweden: "}, {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Finland: "}, {"Helsinki", "Espoo", "Hanko", "Jämsä"}};


        for (String[] i : array) {
            for (String j : i)

                System.out.print(j + ", ");

        }

        System.out.println();


        String[][] array2;
        array2 = new String[3][5];
        array2[0][0] = "Estonia: ";
        array2[0][1] = "Tallinn";
        array2[0][2] = "Tartu";
        array2[0][3] = "Valga";
        array2[0][4] = "Võru";

        array2[1][0] = "Sweden: ";
        array2[1][1] = "Stockholm";
        array2[1][2] = "Uppsala";
        array2[1][3] = "Lund";
        array2[1][4] = "Köping";

        array2[2][0] = "Finland: ";
        array2[2][1] = "Helsinki";
        array2[2][2] = "Espoo";
        array2[2][3] = "Hanko";
        array2[2][4] = "Jämsa";

        for (String[] i : array2) {
            for (String j : i)

                System.out.print(j + ", ");
        }


    }
}



